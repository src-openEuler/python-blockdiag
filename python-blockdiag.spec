%global _empty_manifest_terminate_build 0
Name:           python-blockdiag
Version:        3.0.0
Release:        1
Summary:        blockdiag generates block-diagram image from text
License:        Apache-2.0
URL:            http://blockdiag.com/
Source0:        https://files.pythonhosted.org/packages/b4/eb/e2a4b6d5bf7f7121104ac7a1fc80b5dfa86ba286adbd1f25bf32a090a5eb/blockdiag-3.0.0.tar.gz
BuildArch:      noarch
%description
* Generate block-diagram from dot like text (basic feature).
* Multilingualization for node-label (utf-8 only).

%package -n python3-blockdiag
Summary:        blockdiag generates block-diagram image from text
Provides:       python-blockdiag
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-funcparserlib
BuildRequires:  python3-pillow
BuildRequires:  python3-webcolors
BuildRequires:  python3-reportlab
BuildRequires:  python3-docutils
# General requires
Requires:       python3-setuptools
Requires:       python3-funcparserlib
Requires:       python3-pillow
Requires:       python3-webcolors
Requires:       python3-reportlab
Requires:       python3-docutils
%description -n python3-blockdiag
* Generate block-diagram from dot like text (basic feature).
* Multilingualization for node-label (utf-8 only).

%package help
Summary:        blockdiag generates block-diagram image from text
Provides:       python3-blockdiag-doc
%description help
* Generate block-diagram from dot like text (basic feature).
* Multilingualization for node-label (utf-8 only).

%prep
%autosetup -n blockdiag-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .


%files -n python3-blockdiag -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Jul 04 2022 OpenStack_SIG <openstack@openeuler.org> - 3.0.0-1
- Upgrade package python3-blockdiag to version 3.0.0

* Mon Nov 23 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
